package main

import (
  "gopkg.in/yaml.v2"
  "os"

  homedir "github.com/mitchellh/go-homedir"
)

type ConfigJira struct {
  Url string `yaml:"url"`
  Username string `yaml:"username"`
  Password string `yaml:"password"`
}

type ConfigGitLab struct {
  Url string `yaml:"url"`
  AccessToken string `yaml:"access_token"`
}

type ConfigOffice365 struct {
  ClientId string `yaml:"client_id"`
	TenantId string `yaml:"tenant_id"`
}

type Config struct {
  TimeZone string `yaml:"time_zone"`
  IgnoreMeetings []string `yaml:"ignore_meetings"`
  Jira ConfigJira `yaml:"jira"`
  GitLab ConfigGitLab `yaml:"gitlab"`
	Office365 ConfigOffice365 `yaml:"office365"`
}

func getConfig(filePath string) Config {
  filePath, err := homedir.Expand(filePath)
  if err != nil {
    panic(err)
  }

  file, err := os.Open(filePath)
  if err != nil {
    panic(err)
  }
  defer file.Close()
  
  var config Config
  decoder := yaml.NewDecoder(file)
  err = decoder.Decode(&config)
  if err != nil {
    panic(err)
  }
  
  return config
}
