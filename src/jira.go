package main

import (
  "net/http"
  "encoding/json"
  "regexp"
  "time"
  "encoding/xml"
  "net/http/cookiejar"
  "net/url"

  "io/ioutil"
  "strconv"
)

type JiraEntry struct {
  Text string `xml:"title"`
  Published time.Time `xml:"published"`
}

type JiraFeed struct {
  Entry []JiraEntry `xml:"entry"`
}

func GetJiraClient(config Config) http.Client {
  jar, err := cookiejar.New(nil)
  if err != nil {
    panic(err)
  }
  client := http.Client{Jar: jar}
  _, err = client.PostForm(config.Jira.Url + "/rest/gadget/1.0/login", url.Values{
    "os_username": {config.Jira.Username},
    "os_password": {config.Jira.Password},
  })
  if err != nil {
    panic(err)
  }

  return client
}

func fetchJiraEvents(config Config, client http.Client, day time.Time) JiraFeed {
  from := day.UnixMilli()
  to := day.AddDate(0, 0, 1).UnixMilli()
  resp, err := client.Get(
    config.Jira.Url +
    "/activity?streams=user+IS+" +
    config.Jira.Username +
    "&streams=update-date+BETWEEN+" +
    strconv.FormatInt(from, 10) +
    "+" +
    strconv.FormatInt(to, 10),
  )
  if err != nil {
    panic(err)
  }

  data, err := ioutil.ReadAll(resp.Body)
  resp.Body.Close()
  if err != nil {
    panic(err)
  }

  var jiraFeed JiraFeed
  xml.Unmarshal(data, &jiraFeed)

  return jiraFeed
}

func convertJiraEvent(config Config, jiraEntry JiraEntry) Event {
  regex := regexp.MustCompile(`SCIP-[0-9]+`)
  matches := regex.FindStringSubmatch(jiraEntry.Text)
  var ticket = ""
  if len(matches) != 0 {
    ticket = matches[0]
  }

  location, err := time.LoadLocation(config.TimeZone)
  if err != nil {
    panic(err)
  }

  return Event{
    Ticket: ticket,
    StartAt: jiraEntry.Published.In(location),
    Origin: "Jira",
  }
}

func GetJiraEvents(config Config, client http.Client, day time.Time) []Event {
  var events []Event 
  jiraFeed := fetchJiraEvents(config, client, day)
  for _, jiraEntry := range jiraFeed.Entry {
    events = append(events, convertJiraEvent(config, jiraEntry))
  }

  return events
}

type JiraField struct {
  Summary string `json:"summary"`
}

type JiraIssue struct {
  Fields JiraField `json:"fields"`
}

func GetJiraTicketSmmary(config Config, ticket string, client http.Client) string {
  response, err := client.Get(config.Jira.Url + "/rest/api/2/issue/" + ticket)
  if err != nil {
    panic(err)
  }
  var jiraIssue JiraIssue
  err = json.NewDecoder(response.Body).Decode(&jiraIssue)
  if err != nil {
    panic(err)
  }
  
  return jiraIssue.Fields.Summary
}
