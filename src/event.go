package main

import (
  "time"
  "net/http"
  "slices"
  "sort"
)

type Event struct {
  Ticket string
  Actions []string
  StartAt time.Time
  EndAt time.Time
  Duration float64
  DurationBetweenMeetings float64
  Summary string
  Origin string
}

func removeAdjacentEvents(events []Event) []Event {
  eventResults := []Event{}
  for index, event := range events {
    if index == 0 || event.Origin == "Outlock" || events[index - 1].Ticket != event.Ticket {
      eventResults = append(eventResults, event)
    }

    if index != 0 && events[index - 1].Ticket == event.Ticket {
      lastEvent := &eventResults[len(eventResults) - 1]
      lastEvent.Actions = append(lastEvent.Actions, event.Actions...)
    }
  }

  return eventResults
}

func addSummaryToEvents(events []Event, config Config, client http.Client) []Event {
  for index, event := range events {
    if len(event.Ticket) > 0 {
      events[index].Summary = "#" + event.Ticket + " " + GetJiraTicketSmmary(config, event.Ticket, client)
    }
  }

  return events
}

func sortEvents(events []Event) []Event {
  sort.Slice(events[:], func(i, j int) bool {
    return events[i].StartAt.Before(events[j].StartAt)
  })

  return events
}

func findPreviousMeetingEnd(index int, events []Event) time.Time {
  for i := index; i >= 0; i-- {
    if events[i].Origin == "Outlock" {
      return events[i].EndAt
    }
  }

  startAt := events[index].StartAt
  return  time.Date(startAt.Year(), startAt.Month(), startAt.Day(), 9, 0, 0, startAt.Nanosecond(), startAt.Location())
}

func findNextMeetingStart(index int, events []Event) time.Time {
  for i := index; i < len(events); i++ {
    if events[i].Origin == "Outlock" {
      return events[i].StartAt
    }
  }

  startAt := events[index].StartAt
  return  time.Date(startAt.Year(), startAt.Month(), startAt.Day(), 18, 0, 0, startAt.Nanosecond(), startAt.Location())
}

func durationBetweenMeetings(index int, events []Event) float64 {
  previousMeetingEnd := findPreviousMeetingEnd(index, events)
  nextMeetingStart := findNextMeetingStart(index, events)

  return nextMeetingStart.Sub(previousMeetingEnd).Hours() 
}

func assignDurationBetweenMeetings(events []Event) []Event {
  for index, event := range events {
    if event.Origin != "Outlock" {
      events[index].DurationBetweenMeetings = durationBetweenMeetings(index, events)
    }
  }

  return events
}

func filterEvents(events []Event, config Config) []Event {
  var resultEvents = []Event{}
  for _, event := range events {
    if event.Origin != "Outlock" || !slices.Contains(config.IgnoreMeetings, event.Summary) {
      resultEvents = append(resultEvents, event)
    }
  }
  return resultEvents
}
