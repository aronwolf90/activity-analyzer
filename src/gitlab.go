package main

import (
  "net/http"
  "encoding/json"
  "regexp"
  "time"
)

type GitlabPushData struct {
  CommitTitle string `json:"commit_title"`
  Ref string `json:"ref"`
}

type GitlabEvent struct {
  CreatedAt time.Time `json:"created_at"`
  ActionName string `json:"action_name"`
  TargetTitle string `json:"target_title"`
  PushData GitlabPushData `json:"push_data"`
}

func getGitlabEventsFromApi(config Config, day time.Time) []GitlabEvent {
  after := day.AddDate(0, 0, -1).Format("2006-01-02")
  before := day.AddDate(0, 0, 1).Format("2006-01-02")
  request, err := http.NewRequest(
    "GET",
    config.GitLab.Url + "events?per_page=100&after=" + after + "&before=" + before,
    nil,
  )
  if err != nil {
    panic(err)
  }
  request.Header.Add("PRIVATE-TOKEN", config.GitLab.AccessToken) 
  client := &http.Client{}
  response, err := client.Do(request)
  if err != nil {
    panic(err)
  }

  var gitlabEvents []GitlabEvent
  err = json.NewDecoder(response.Body).Decode(&gitlabEvents)
  if err != nil {
    panic(err)
  }

  return gitlabEvents
}

func convertGitlabEvent(config Config, gitlabEvent GitlabEvent) Event {
  location, err := time.LoadLocation(config.TimeZone)
  if err != nil {
    panic(err)
  }

  regex := regexp.MustCompile(`SCIP-[0-9]+`)
  matches := regex.FindStringSubmatch(gitlabEvent.TargetTitle + gitlabEvent.PushData.CommitTitle + gitlabEvent.PushData.Ref)
  var ticket = ""
  if len(matches) != 0 {
    ticket = matches[0]
  }
  
  return Event{
    Ticket: ticket,
    Actions: []string{gitlabEvent.ActionName},
    StartAt: gitlabEvent.CreatedAt.In(location),
    Origin: "GitLab",
  }
}

func GetGitlabEvents(config Config, day time.Time) []Event {
  var events []Event 
  for _, gitlabEvent := range getGitlabEventsFromApi(config, day) {
    events = append(events, convertGitlabEvent(config, gitlabEvent))
  }

  return events
}
