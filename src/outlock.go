package main

import (
  "fmt"
  "time"

  "context"
  msgraphsdk "github.com/microsoftgraph/msgraph-sdk-go"
  graphusers "github.com/microsoftgraph/msgraph-sdk-go/users"
  models "github.com/microsoftgraph/msgraph-sdk-go/models"
  "github.com/Azure/azure-sdk-for-go/sdk/azidentity"
)

func GetCalenderEvents(config Config, day time.Time) []Event {
	// Create the device code credential
	credential, err := azidentity.NewDeviceCodeCredential(&azidentity.DeviceCodeCredentialOptions{
		ClientID: config.Office365.ClientId,
		TenantID: config.Office365.TenantId,
		UserPrompt: func(ctx context.Context, message azidentity.DeviceCodeMessage) error {
			fmt.Println(message.Message)
			return nil
		},
	})
	if err != nil {
		panic(err)
	}

	// Create an auth provider using the credential
	graphClient, err := msgraphsdk.NewGraphServiceClientWithCredentials(credential, []string{"user.read", "calendars.read"})
  if err != nil {
    panic(err)
  }
  requestStartDateTime := day.AddDate(0, 0, 0).Format(time.RFC3339Nano)
  requestEndDateTime := day.AddDate(0, 0, 1).Format(time.RFC3339Nano)

  requestParameters := &graphusers.ItemCalendarViewRequestBuilderGetQueryParameters{
  	StartDateTime: &requestStartDateTime,
  	EndDateTime: &requestEndDateTime,
  }
  configuration := &graphusers.ItemCalendarViewRequestBuilderGetRequestConfiguration{
  	QueryParameters: requestParameters,
  }
  
  calendarView, err := graphClient.Me().CalendarView().Get(context.Background(), configuration)
  if err != nil {
    panic(err)
  }

  var events []Event
  for _, message := range calendarView.GetValue() {
    event := CreateEventFromMessage(config, message)
    events = append(events, event)
	}
  return events
}

func CreateEventFromMessage(config Config, message models.Eventable) Event {
  location, err := time.LoadLocation(config.TimeZone)
  startAt, err := time.Parse("2006-01-02T15:04:05.999999999", *message.GetStart().GetDateTime())
  if err != nil {
    panic(err)
  }
  startAt = startAt.In(location)

  endAt, err := time.Parse("2006-01-02T15:04:05.999999999", *message.GetEnd().GetDateTime())
  if err != nil {
    panic(err)
  }
  endAt = endAt.In(location)

  return Event{
    StartAt: startAt,
    EndAt: endAt,
    Duration: endAt.Sub(startAt).Hours(),
    Summary: *message.GetSubject(),
    Origin: "Outlock",
  }
}
