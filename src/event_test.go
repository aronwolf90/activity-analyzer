package main

import (
  "testing"
  "time"

  "github.com/jarcoal/httpmock"
  "github.com/stretchr/testify/assert"
)

var configEvents = Config{
  TimeZone: "Europe/Berlin",
  IgnoreMeetings: []string{"Do not disturb - Donnerstag"},
  Jira: ConfigJira{
    Url: "https://jira.example.com",
    Username: "user@example.com",
    Password: "testtest",
  },
  GitLab: ConfigGitLab{
    Url: "https://gitlab.com/api/v4/",
    AccessToken: "testtest",
  },
  Office365: ConfigOffice365{
    ClientId: "ffffffff-ffff-ffff-ffff-ffffffffffff",
    TenantId: "common",
  },
}

func TestAddSummaryToEvents(t *testing.T) {
  httpmock.Activate()
  defer httpmock.DeactivateAndReset()

  response := `{
    "expand":"renderedFields,names,schema,operations,editmeta,changelog,versionedRepresentations",
    "id":"111623",
    "self":"https://jira.example.net/rest/api/2/issue/1",
    "key":"SCIP-1000",
    "fields":{
       "fixVersions":[
          
       ],
       "priority":{
          "self":"https://jira.example.net/rest/api/2/priority/3",
          "iconUrl":"https://jira.example.net/images/icons/priorities/major.svg",
          "name":"major",
          "id":"3"
       },
       "labels":[
          
       ],
       "aggregatetimeoriginalestimate":null,
       "timeestimate":null,
       "versions":[
          
       ],
       "issuelinks":[
          
       ],
       "customfield_11709":null,
       "assignee":{
          "self":"https://jira.example.net/rest/api/2/user?username=user%40example.com",
          "name":"user@example.com",
          "key":"user@example.com",
          "emailAddress":"user@example.com",
          "avatarUrls":{
             "48x48":"https://jira.example.net/secure/useravatar?avatarId=10122",
             "24x24":"https://jira.example.net/secure/useravatar?size=small&avatarId=1",
             "16x16":"https://jira.example.net/secure/useravatar?size=xsmall&avatarId=1",
             "32x32":"https://jira.example.net/secure/useravatar?size=medium&avatarId=1"
          },
          "displayName":"Test Test",
          "active":true,
          "timeZone":"Europe/Berlin"
       },
       "status":{
          "self":"https://jira.example.net/rest/api/2/status/3",
          "description":"Vorgang ist in Umsetzung",
          "iconUrl":"https://jira.example.net/images/icons/statuses/inprogress.png",
          "name":"in progress",
          "id":"3",
          "statusCategory":{
             "self":"https://jira.example.net/rest/api/2/statuscategory/4",
             "id":4,
             "key":"indeterminate",
             "colorName":"yellow",
             "name":"In Arbeit"
          }
       },
       "components":[
          {
             "self":"https://jira.example.net/rest/api/2/component/1",
             "id":"12301",
             "name":"SCIP Core  - Board"
          }
       ],
       "aggregatetimeestimate":null,
       "creator":{
          "self":"https://jira.example.net/rest/api/2/user?username=user2@example.com",
          "name":"user@example.com",
          "key":"user@example.com",
          "emailAddress":"user@example.com",
          "avatarUrls":{
             "48x48":"https://jira.example.net/secure/useravatar?ownerId=joschka.schulz%40example.com&avatarId=16208",
             "24x24":"https://jira.example.net/secure/useravatar?size=small&ownerId=joschka.schulz%40example.com&avatarId=16208",
             "16x16":"https://jira.example.net/secure/useravatar?size=xsmall&ownerId=joschka.schulz%40example.com&avatarId=16208",
             "32x32":"https://jira.example.net/secure/useravatar?size=medium&ownerId=joschka.schulz%40example.com&avatarId=16208"
          },
          "displayName":"User2",
          "active":true,
          "timeZone":"Europe/Berlin"
       },
       "subtasks":[
          
       ],
       "reporter":{
          "self":"https://jira.example.net/rest/api/2/user?username=user2%40example.com",
          "name":"user2@example.com",
          "key":"user2@example.com",
          "emailAddress":"user2@example.com",
          "avatarUrls":{
             "48x48":"https://jira.example.net/secure/useravatar?ownerId=user2%40example.com&avatarId=2",
             "24x24":"https://jira.example.net/secure/useravatar?size=small&ownerId=joschka.schulz%40example.com&avatarId=2",
             "16x16":"https://jira.example.net/secure/useravatar?size=xsmall&ownerId=joschka.schulz%40example.com&avatarId=2",
             "32x32":"https://jira.example.net/secure/useravatar?size=medium&ownerId=joschka.schulz%40example.com&avatarId=2"
          },
          "displayName":"User2",
          "active":true,
          "timeZone":"Europe/Berlin"
       },
       "aggregateprogress":{
          "progress":0,
          "total":0
       },
       "progress":{
          "progress":0,
          "total":0
       },
       "worklog":{
          "startAt":0,
          "maxResults":20,
          "total":0,
          "worklogs":[
             
          ]
       },
       "issuetype":{
          "self":"https://jira.example.net/rest/api/2/issuetype/10001",
          "id":"10001",
          "description":"Von Jira Software erstellt – nicht bearbeiten oder löschen. Vorgangstyp für eine User Story.",
          "iconUrl":"https://jira.example.net/secure/viewavatar?size=xsmall&avatarId=11415&avatarType=issuetype",
          "name":"Story",
          "subtask":false,
          "avatarId":3
       },
       "timespent":null,
       "project":{
          "self":"https://jira.example.net/rest/api/2/project/1",
          "id":"1",
          "key":"SCIP",
          "name":"SCIP",
          "projectTypeKey":"software",
          "avatarUrls":{
             "48x48":"https://jira.example.net/secure/projectavatar?avatarId=11424",
             "24x24":"https://jira.example.net/secure/projectavatar?size=small&avatarId=11424",
             "16x16":"https://jira.example.net/secure/projectavatar?size=xsmall&avatarId=11424",
             "32x32":"https://jira.example.net/secure/projectavatar?size=medium&avatarId=11424"
          },
          "projectCategory":{
             "self":"https://jira.example.net/rest/api/2/projectCategory/10000",
             "id":"10000",
             "description":"Whatever.",
             "name":"Versicherung (Insurance)"
          }
       },
       "resolutiondate":null,
       "workratio":-1,
       "watches":{
          "self":"https://jira.example.net/rest/api/2/issue/SCIP-1000/watchers",
          "watchCount":1,
          "isWatching":false
       },
       "created":"2022-11-29T17:55:30.000+0100",
       "updated":"2023-08-30T13:25:19.000+0200",
       "timeoriginalestimate":null,
       "description":null,
       "timetracking":{
          
       },
       "attachment":[
          {
             "self":"https://jira.example.net/rest/api/2/attachment/2",
             "id":"104806",
             "filename":"image-2022-11-29-17-31-09-455.png",
             "author":{
                "self":"https://jira.example.net/rest/api/2/user?username=user2%40example.com",
                "name":"user2@example.com",
                "key":"user2@example.com",
                "emailAddress":"user2@example.com",
                "avatarUrls":{
                   "48x48":"https://jira.example.net/secure/useravatar?ownerId=user2%40example.com&avatarId=1",
                   "24x24":"https://jira.example.net/secure/useravatar?size=small&ownerId=user2%40example.com&avatarId=2",
                   "16x16":"https://jira.example.net/secure/useravatar?size=xsmall&ownerId=user2%40example.com&avatarId=2",
                   "32x32":"https://jira.example.net/secure/useravatar?size=medium&ownerId=user2%40example.com&avatarId=16208"
                },
                "displayName":"User2",
                "active":true,
                "timeZone":"Europe/Berlin"
             },
             "created":"2022-11-29T17:31:10.000+0100",
             "size":95917,
             "mimeType":"image/png",
             "content":"https://jira.example.net/secure/attachment/2/image-2022-11-29-17-31-09-455.png",
             "thumbnail":"https://jira.example.net/secure/thumbnail/2/_thumb_104806.png"
          }
       ],
       "summary":"Whatever",
       "comment":{
          "comments":[
             
          ],
          "maxResults":0,
          "total":0,
          "startAt":0
       }
    }
  }`

  httpmock.RegisterResponder(
    "POST",
    "/rest/gadget/1.0/login",
    httpmock.NewStringResponder(200, ""),
  )
  httpmock.RegisterResponder(
    "GET",
    "/rest/api/2/issue/SCIP-1000",
    httpmock.NewStringResponder(200, response),
  )

  jiraClient := GetJiraClient(configJira)
  events := []Event{Event{ Ticket: "SCIP-1000" }}
  expectedResult := []Event{Event{ Ticket: "SCIP-1000", Summary: "#SCIP-1000 Whatever" }}
  addSummaryToEvents(events, configEvents, jiraClient)
  assert.Equal(t, events, expectedResult)
}

func TestSortEvents(t *testing.T) {
  events := []Event{
    Event{ Ticket: "SCIP-1000", StartAt: time.Date(2023, time.September, 2, 14, 39, 32, 369000000, time.UTC)},
    Event{ Ticket: "SCIP-1001", StartAt: time.Date(2023, time.September, 1, 14, 39, 32, 369000000, time.UTC)},
    Event{ Ticket: "SCIP-1002", StartAt: time.Date(2023, time.September, 3, 14, 39, 32, 369000000, time.UTC)},
  }
  expectedResult := []Event{
    Event{ Ticket: "SCIP-1001", StartAt: time.Date(2023, time.September, 1, 14, 39, 32, 369000000, time.UTC)},
    Event{ Ticket: "SCIP-1000", StartAt: time.Date(2023, time.September, 2, 14, 39, 32, 369000000, time.UTC)},
    Event{ Ticket: "SCIP-1002", StartAt: time.Date(2023, time.September, 3, 14, 39, 32, 369000000, time.UTC)},
  }

  events = sortEvents(events)
  assert.Equal(t, events, expectedResult)
}

func TestFindPreviousMeetingEnd(t *testing.T) {
  events := []Event{
    Event{
      Ticket: "SCIP-1001",
      StartAt: time.Date(2023, time.September, 1, 9, 39, 32, 369000000, time.UTC),
      EndAt: time.Date(2023, time.September, 1, 10, 39, 32, 369000000, time.UTC),
      Origin: "Outlock",
    },
    Event{
      Ticket: "SCIP-1000",
      StartAt: time.Date(2023, time.September, 1, 11, 39, 32, 369000000, time.UTC),
      Origin: "GitLab",
    },
    Event{
      Ticket: "SCIP-1002",
      StartAt: time.Date(2023, time.September, 1, 12, 39, 32, 369000000, time.UTC),
      EndAt: time.Date(2023, time.September, 1, 13, 39, 32, 369000000, time.UTC),
      Origin: "Outlock",
    },
  }

  duration := findPreviousMeetingEnd(1, events)
  expectedResult := time.Date(2023, time.September, 1, 10, 39, 32, 369000000, time.UTC)
  assert.Equal(t, duration, expectedResult)
}

func TestFindNextMeetingStart(t *testing.T) {
  events := []Event{
    Event{
      Ticket: "SCIP-1001",
      StartAt: time.Date(2023, time.September, 1, 9, 39, 32, 369000000, time.UTC),
      EndAt: time.Date(2023, time.September, 1, 10, 39, 32, 369000000, time.UTC),
      Origin: "Outlock",
    },
    Event{
      Ticket: "SCIP-1000",
      StartAt: time.Date(2023, time.September, 1, 11, 39, 32, 369000000, time.UTC),
      Origin: "GitLab",
    },
    Event{
      Ticket: "SCIP-1002",
      StartAt: time.Date(2023, time.September, 1, 12, 39, 32, 369000000, time.UTC),
      EndAt: time.Date(2023, time.September, 1, 13, 39, 32, 369000000, time.UTC),
      Origin: "Outlock",
    },
  }

  duration := findNextMeetingStart(1, events)
  expectedResult := time.Date(2023, time.September, 1, 12, 39, 32, 369000000, time.UTC)
  assert.Equal(t, duration, expectedResult)
}

func TestDurationBetweenMeetings(t *testing.T) {
  events := []Event{
    Event{
      Ticket: "SCIP-1001",
      StartAt: time.Date(2023, time.September, 1, 9, 39, 32, 369000000, time.UTC),
      EndAt: time.Date(2023, time.September, 1, 10, 39, 32, 369000000, time.UTC),
      Origin: "Outlock",
    },
    Event{
      Ticket: "SCIP-1000",
      StartAt: time.Date(2023, time.September, 1, 11, 39, 32, 369000000, time.UTC),
      Origin: "GitLab",
    },
    Event{
      Ticket: "SCIP-1002",
      StartAt: time.Date(2023, time.September, 1, 12, 39, 32, 369000000, time.UTC),
      EndAt: time.Date(2023, time.September, 1, 13, 39, 32, 369000000, time.UTC),
      Origin: "Outlock",
    },
  }

  duration := durationBetweenMeetings(1, events)
  assert.Equal(t, duration, 2.0)
}

func TestAssignDurationBetweenMeetings(t *testing.T) {
  events := []Event{
    Event{
      Ticket: "SCIP-1001",
      StartAt: time.Date(2023, time.September, 1, 9, 39, 32, 369000000, time.UTC),
      EndAt: time.Date(2023, time.September, 1, 10, 39, 32, 369000000, time.UTC),
      Origin: "Outlock",
    },
    Event{
      Ticket: "SCIP-1000",
      StartAt: time.Date(2023, time.September, 1, 11, 39, 32, 369000000, time.UTC),
      Origin: "GitLab",
    },
    Event{
      Ticket: "SCIP-1002",
      StartAt: time.Date(2023, time.September, 1, 12, 39, 32, 369000000, time.UTC),
      EndAt: time.Date(2023, time.September, 1, 13, 39, 32, 369000000, time.UTC),
      Origin: "Outlock",
    },
  }
  expectResult := []Event{
    Event{
      Ticket: "SCIP-1001",
      StartAt: time.Date(2023, time.September, 1, 9, 39, 32, 369000000, time.UTC),
      EndAt: time.Date(2023, time.September, 1, 10, 39, 32, 369000000, time.UTC),
      Origin: "Outlock",
    },
    Event{
      Ticket: "SCIP-1000",
      StartAt: time.Date(2023, time.September, 1, 11, 39, 32, 369000000, time.UTC),
      DurationBetweenMeetings: 2.0,
      Origin: "GitLab",
    },
    Event{
      Ticket: "SCIP-1002",
      StartAt: time.Date(2023, time.September, 1, 12, 39, 32, 369000000, time.UTC),
      EndAt: time.Date(2023, time.September, 1, 13, 39, 32, 369000000, time.UTC),
      Origin: "Outlock",
    },
  }

  result := assignDurationBetweenMeetings(events)
  assert.Equal(t, result, expectResult)
}

func TestFilterEvents(t *testing.T) {
  events := []Event{
    Event{
      Ticket: "SCIP-1001",
      StartAt: time.Date(2023, time.September, 1, 9, 39, 32, 369000000, time.UTC),
      EndAt: time.Date(2023, time.September, 1, 10, 39, 32, 369000000, time.UTC),
      Origin: "Outlock",
    },
    Event{
      Ticket: "SCIP-1002",
      StartAt: time.Date(2023, time.September, 1, 12, 39, 32, 369000000, time.UTC),
      EndAt: time.Date(2023, time.September, 1, 13, 39, 32, 369000000, time.UTC),
      Summary: "Do not disturb - Donnerstag",
      Origin: "Outlock",
    },
  }
  expectResult := []Event{
    Event{
      Ticket: "SCIP-1001",
      StartAt: time.Date(2023, time.September, 1, 9, 39, 32, 369000000, time.UTC),
      EndAt: time.Date(2023, time.September, 1, 10, 39, 32, 369000000, time.UTC),
      Origin: "Outlock",
    },
  }

  result := filterEvents(events, configEvents)
  assert.Equal(t, result, expectResult)
}
