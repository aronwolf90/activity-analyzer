package main

import (
  "testing"

  "github.com/stretchr/testify/assert"
)

func TestGetConfig(t *testing.T) {
  config := getConfig("testdata/config.yaml")

  expectedResult := Config{
    TimeZone: "Europe/Berlin",
    IgnoreMeetings: []string{"Do not disturb - Donnerstag"},
    Jira: ConfigJira{
      Url: "https://jira.example.net",
      Username: "user@example.com",
      Password: "Testtest",
    },
    GitLab: ConfigGitLab{
      Url: "https://gitlab.com/api/v4/",
      AccessToken: "yyyyy-yyyyyyyyyyyyyyyyyyyy",
    },
    Office365: ConfigOffice365{
      ClientId: "ffffffff-ffff-ffff-ffff-ffffffffffff",
      TenantId: "common",
    },
  }
  assert.Equal(t, config, expectedResult)
}
