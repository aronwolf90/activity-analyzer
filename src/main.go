package main

import (
  "fmt"
  "encoding/json"
  "time"
  "os"
)

var configFile = "~/.config/activity-analyzer.yaml"  

func main() {
  if len(os.Args) < 2 {
    fmt.Fprintf(os.Stderr, "Wrong arguments, use: activity-analyzer YYYY-MM-DD")
    os.Exit(2)
  }

  day, err := time.Parse("2006-01-02", os.Args[1])
  if err != nil {
    fmt.Fprintf(os.Stderr, "Wrong arguments, use: activity-analyzer YYYY-MM-DD")
    os.Exit(2)
  }

  config := getConfig(configFile)

  jiraClient := GetJiraClient(config)
  events := append(GetGitlabEvents(config, day), GetJiraEvents(config, jiraClient, day)...)
  events = append(events, GetCalenderEvents(config, day)...)
 
  events = sortEvents(events) 
  events = removeAdjacentEvents(events)
  events = addSummaryToEvents(events, config, jiraClient)
  events = filterEvents(events, config)
  events = assignDurationBetweenMeetings(events) 
 
  empJSON, err := json.MarshalIndent(events, "", "  ")
 
  if err != nil {
    panic(err)
  }
 
  fmt.Println(string(empJSON))
}
