package main

import (
  "testing"
  "time"

  "github.com/jarcoal/httpmock"
  "github.com/stretchr/testify/assert"
)

var configGitLab = Config{
  TimeZone: "Europe/Berlin",
  Jira: ConfigJira{
    Url: "https://jira.example.com",
    Username: "user@example.com",
    Password: "testtest",
  },
  GitLab: ConfigGitLab{
    Url: "https://gitlab.com/api/v4/",
    AccessToken: "testtest",
  },
  Office365: ConfigOffice365{
    ClientId: "ffffffff-ffff-ffff-ffff-ffffffffffff",
    TenantId: "common",
  },
}

func TestGetGitlabEventsFromApi(t *testing.T) {
  httpmock.Activate()
  defer httpmock.DeactivateAndReset()

  response :=
  `[
     {
       "id": 1,
       "project_id": 1,
       "action_name":"pushed to",
       "target_id":null,
       "target_iid":null,
       "target_type":null,
       "author_id": 1,
       "target_title": null,
       "created_at":"2023-09-01T14:39:32.369Z",
       "author":{
         "id": 1,
         "username":"test",
         "name":"Test test",
         "state":"active",
         "avatar_url":"https://gitlab.com/uploads/-/system/user/avatar/1/avatar.png",
         "web_url":"https://gitlab.com/test"
       },
       "push_data": {
         "commit_count":1,
         "action":"pushed",
         "ref_type":"branch",
         "commit_from":
         "4e2759c256cddsafdafsafafd2d3c8b486e6e",
         "commit_to":"41dadfadfafddsafafcc9bfb533989dab031",
         "ref":"fix/SCIP-8520-whatever",
         "commit_title":"fix(SCIP-8520): whatever",
         "ref_count":null
       },
       "author_username":"test"
     }
  ]`

  httpmock.RegisterResponder(
    "GET",
    "/api/v4/events?per_page=100&after=2023-08-31&before=2023-09-02",
    httpmock.NewStringResponder(200, response),
  )

  gitlabEvent := getGitlabEventsFromApi(
    configGitLab,
    time.Date(2023, 9, 01, 0, 0, 0, 0, time.Local),
  )

  expectedResult := []GitlabEvent(
    []GitlabEvent{
      GitlabEvent{
        CreatedAt:time.Date(2023, time.September, 1, 14, 39, 32, 369000000, time.UTC),
        TargetTitle:"",
        ActionName: "pushed to", 
        PushData: GitlabPushData{
          CommitTitle:"fix(SCIP-8520): whatever",
          Ref:"fix/SCIP-8520-whatever",
        },
      },
    },
  )

  assert.Equal(t, gitlabEvent, expectedResult)
}

func TestConvertGitlabEvent(t *testing.T) {
  gitlabEvent := GitlabEvent{
    CreatedAt: time.Date(2023, 9, 01, 10, 15, 34, 0, time.Local),
    ActionName: "pushed to", 
    TargetTitle: "fix(SCIP-8518) spec helper CamtCreator for RiskCarrier",
    PushData: GitlabPushData{
      CommitTitle: "",
      Ref: "",
    }, 
  }
  event := convertGitlabEvent(configGitLab, gitlabEvent)

  location, err := time.LoadLocation("Europe/Berlin")
  if err != nil {
    panic(err)
  }

  expectedResult := Event{
    Ticket: "SCIP-8518",
    Actions: []string{"pushed to"},
    StartAt: time.Date(2023, time.September, 1, 12, 15, 34, 0, location),
    Origin: "GitLab",
  }
  assert.Equal(t, event, expectedResult)
}

func TestGetGitlabEvents(t *testing.T) {
  httpmock.Activate()
  defer httpmock.DeactivateAndReset()

  response :=
  `[
     {
       "id": 1,
       "project_id": 1,
       "action_name":"pushed to",
       "target_id":null,
       "target_iid":null,
       "target_type":null,
       "author_id": 1,
       "target_title": null,
       "created_at":"2023-09-01T14:39:32.369Z",
       "author":{
         "id": 1,
         "username":"test",
         "name":"Test test",
         "state":"active",
         "avatar_url":"https://gitlab.com/uploads/-/system/user/avatar/1/avatar.png",
         "web_url":"https://gitlab.com/test"
       },
       "push_data": {
         "commit_count":1,
         "action":"pushed",
         "ref_type":"branch",
         "commit_from":
         "4e2759c256cddsafdafsafafd2d3c8b486e6e",
         "commit_to":"41dadfadfafddsafafcc9bfb533989dab031",
         "ref":"fix/SCIP-8520-whatever",
         "commit_title":"fix(SCIP-8520): whatever",
         "ref_count":null
       },
       "author_username":"test"
     }
  ]`

  httpmock.RegisterResponder(
    "GET",
    "/api/v4/events?per_page=100&after=2023-08-31&before=2023-09-02",
    httpmock.NewStringResponder(200, response),
  )
  events := GetGitlabEvents(configGitLab, time.Date(2023, 9, 01, 0, 0, 0, 0, time.Local))

  location, err := time.LoadLocation("Europe/Berlin")
  if err != nil {
    panic(err)
  }
  expectedResult := []Event{
    Event{
      Ticket: "SCIP-8520",
      Actions: []string{"pushed to"},
      StartAt: time.Date(2023, time.September, 1, 16, 39, 32, 369000000, location),
      Origin: "GitLab",
    },
  }
  assert.Equal(t, events, expectedResult)
}
