package main

import (
  "testing"
  "time"

  "github.com/jarcoal/httpmock"
  "github.com/stretchr/testify/assert"
)

var configJira = Config{
  TimeZone: "Europe/Berlin",
  Jira: ConfigJira{
    Url: "https://jira.example.com",
    Username: "user@example.com",
    Password: "testtest",
  },
  GitLab: ConfigGitLab{
    Url: "https://gitlab.com/api/v4/",
    AccessToken: "testtest",
  },
  Office365: ConfigOffice365{
    ClientId: "ffffffff-ffff-ffff-ffff-ffffffffffff",
    TenantId: "common",
  },
}

func TestFetchJiraEvents(t *testing.T) {
  httpmock.Activate()
  defer httpmock.DeactivateAndReset()

  response :=
  `<feed
	xmlns="http://www.w3.org/2005/Atom"
	xmlns:atlassian="http://streams.atlassian.com/syndication/general/1.0">
	<id>https://jira.example.net/activity?streams=user+IS+user%40example.com&amp;os_authType=basic</id>
	<link href="https://jira.example.net/activity?streams=user+IS+user%40example.com&amp;os_authType=basic" rel="self"/>
	<title type="text">Aktivitätsströme</title>
	<atlassian:timezone-offset>+0200</atlassian:timezone-offset>
	<updated>2023-09-03T08:59:33.575Z</updated>
	<entry
		xmlns:activity="http://activitystrea.ms/spec/1.0/"
		xmlns:thr="http://purl.org/syndication/thread/1.0">
		<id>urn:uuid:7f969a76-5739-3ba2-b62e-5a6fff218972</id>
		<title type="html">&lt;a href="https://jira.example.net/secure/ViewProfile.jspa?name=user%40example.com" class="activity-item-user activity-item-author">Aron Wolf&lt;/a> kommentierte       &lt;a href="https://jira.example.net/browse/SCIP-1000">&lt;span class='resolved-link'>SCIP-1000&lt;/span> - Whatever&lt;/a></title>
		<content type="html">&lt;blockquote>    &lt;p>&lt;a class="user-hover" href="https://jira.example.net/secure/ViewProfile.jspa?name=user2%40example.com">Dominik Paur&lt;/a> Wie im Mattermost besprochen, habe ich einen Termin für den Montag erstellt.&lt;/p>   &lt;/blockquote></content>
		<author
			xmlns:usr="http://streams.atlassian.com/syndication/username/1.0">
			<name>Aron Wolf</name>
			<email>user@example.com</email>
			<uri>https://jira.example.net/secure/ViewProfile.jspa?name=user%40example.com</uri>
			<link
				xmlns:media="http://purl.org/syndication/atommedia" rel="photo" href="https://jira.example.net/secure/useravatar?avatarId=10122&amp;s=16" media:height="16" media:width="16"/>
				<link
					xmlns:media="http://purl.org/syndication/atommedia" rel="photo" href="https://jira.example.net/secure/useravatar?avatarId=10122&amp;s=48" media:height="48" media:width="48"/>
					<usr:username>user@example.com</usr:username>
					<activity:object-type>http://activitystrea.ms/schema/1.0/person</activity:object-type>
				</author>
				<published>2023-09-01T17:00:23.000Z</published>
				<updated>2023-09-01T17:00:23.000Z</updated>
				<category term="comment"/>
				<link href="https://jira.example.net/browse/SCIP-1000?focusedCommentId=318826&amp;page=com.atlassian.jira.plugin.system.issuetabpanels:comment-tabpanel#comment-318826" rel="alternate"/>
				<link href="https://jira.example.net/secure/viewavatar?size=xsmall&amp;avatarId=11403&amp;avatarType=issuetype" rel="http://streams.atlassian.com/syndication/icon" title="Bug"/>
				<link href="https://jira.example.net/s/qxtlpn/813018/1am8j4d/1.0/_/download/resources/jira.webresources:global-static/wiki-renderer.css" rel="http://streams.atlassian.com/syndication/css"/>
				<link href="https://jira.example.net/plugins/servlet/streamscomments/issues/SCIP-1000" rel="http://streams.atlassian.com/syndication/reply-to"/>
				<thr:in-reply-to ref="urn:uuid:208b5ad6-cf66-3c3e-80b8-7fbd3fbad101"/>
				<generator uri="https://jira.example.net"/>
				<atlassian:application>com.atlassian.jira</atlassian:application>
				<activity:verb>http://activitystrea.ms/schema/1.0/post</activity:verb>
				<activity:object>
					<id>urn:uuid:aa7889c1-b669-34f4-87a1-6582bf35514d</id>
					<link rel="alternate" href="https://jira.example.net/browse/SCIP-1000?focusedCommentId=318826&amp;page=com.atlassian.jira.plugin.system.issuetabpanels:comment-tabpanel#comment-318826"/>
					<activity:object-type>http://activitystrea.ms/schema/1.0/comment</activity:object-type>
				</activity:object>
				<activity:target>
					<id>urn:uuid:208b5ad6-cf66-3c3e-80b8-7fbd3fbad101</id>
					<title type="text">SCIP-1000</title>
					<summary type="text">Whatever</summary>
					<link rel="alternate" href="https://jira.example.net/browse/SCIP-1000"/>
					<activity:object-type>http://streams.atlassian.com/syndication/types/issue</activity:object-type>
				</activity:target>
				<atlassian:timezone-offset>+0200</atlassian:timezone-offset>
			</entry>
    </feed>`

  httpmock.RegisterResponder(
    "POST",
    "/rest/gadget/1.0/login",
    httpmock.NewStringResponder(200, ""),
  )

  httpmock.RegisterResponder(
    "GET",
    "/activity?streams=user+IS+user@example.com&streams=update-date+BETWEEN+1693526400000+1693612800000",
    httpmock.NewStringResponder(200, response),
  )

  jiraFeed := fetchJiraEvents(
    configJira,
    GetJiraClient(configJira),
    time.Date(2023, 9, 01, 0, 0, 0, 0, time.Local),
  )

  expectedResult := JiraFeed(
    JiraFeed{
      Entry: []JiraEntry{
        JiraEntry{
          Text:"<a href=\"https://jira.example.net/secure/ViewProfile.jspa?name=user%40example.com\" class=\"activity-item-user activity-item-author\">Aron Wolf</a> kommentierte       <a href=\"https://jira.example.net/browse/SCIP-1000\"><span class='resolved-link'>SCIP-1000</span> - Whatever</a>", 
          Published:time.Date(2023, time.September, 1, 17, 0, 23, 0, time.UTC),
        },
      },
    },
  )

  assert.Equal(t, jiraFeed, expectedResult)
}


func TestConvertJiraEvent(t *testing.T) {
  jiraEntry := JiraEntry{
    Text:"<a href=\"https://jira.example.net/secure/ViewProfile.jspa?name=user%40example.com\" class=\"activity-item-user activity-item-author\">Aron Wolf</a> kommentierte       <a href=\"https://jira.example.net/browse/SCIP-1000\"><span class='resolved-link'>SCIP-1000</span> - Whatever</a>", 
    Published: time.Date(2023, time.September, 1, 17, 0, 23, 0, time.UTC),
  }

  location, err := time.LoadLocation("Europe/Berlin")
  if err != nil {
    panic(err)
  }

  event := convertJiraEvent(configJira, jiraEntry)

  expectedResult := Event{
    Ticket: "SCIP-1000",
    StartAt: time.Date(2023, time.September, 1, 19, 0, 23, 0, location),
    Origin: "Jira",
  }
  assert.Equal(t, event, expectedResult)
}

func TestGetJiraTicketSmmary(t *testing.T) {
  httpmock.Activate()
  defer httpmock.DeactivateAndReset()

  response := `{
    "expand":"renderedFields,names,schema,operations,editmeta,changelog,versionedRepresentations",
    "id":"111623",
    "self":"https://jira.example.net/rest/api/2/issue/1",
    "key":"SCIP-1000",
    "fields":{
       "fixVersions":[
          
       ],
       "priority":{
          "self":"https://jira.example.net/rest/api/2/priority/3",
          "iconUrl":"https://jira.example.net/images/icons/priorities/major.svg",
          "name":"major",
          "id":"3"
       },
       "labels":[
          
       ],
       "aggregatetimeoriginalestimate":null,
       "timeestimate":null,
       "versions":[
          
       ],
       "issuelinks":[
          
       ],
       "customfield_11709":null,
       "assignee":{
          "self":"https://jira.example.net/rest/api/2/user?username=user%40example.com",
          "name":"user@example.com",
          "key":"user@example.com",
          "emailAddress":"user@example.com",
          "avatarUrls":{
             "48x48":"https://jira.example.net/secure/useravatar?avatarId=10122",
             "24x24":"https://jira.example.net/secure/useravatar?size=small&avatarId=1",
             "16x16":"https://jira.example.net/secure/useravatar?size=xsmall&avatarId=1",
             "32x32":"https://jira.example.net/secure/useravatar?size=medium&avatarId=1"
          },
          "displayName":"Test Test",
          "active":true,
          "timeZone":"Europe/Berlin"
       },
       "status":{
          "self":"https://jira.example.net/rest/api/2/status/3",
          "description":"Vorgang ist in Umsetzung",
          "iconUrl":"https://jira.example.net/images/icons/statuses/inprogress.png",
          "name":"in progress",
          "id":"3",
          "statusCategory":{
             "self":"https://jira.example.net/rest/api/2/statuscategory/4",
             "id":4,
             "key":"indeterminate",
             "colorName":"yellow",
             "name":"In Arbeit"
          }
       },
       "components":[
          {
             "self":"https://jira.example.net/rest/api/2/component/1",
             "id":"12301",
             "name":"SCIP Core  - Board"
          }
       ],
       "aggregatetimeestimate":null,
       "creator":{
          "self":"https://jira.example.net/rest/api/2/user?username=user2@example.com",
          "name":"user@example.com",
          "key":"user@example.com",
          "emailAddress":"user@example.com",
          "avatarUrls":{
             "48x48":"https://jira.example.net/secure/useravatar?ownerId=joschka.schulz%40example.com&avatarId=16208",
             "24x24":"https://jira.example.net/secure/useravatar?size=small&ownerId=joschka.schulz%40example.com&avatarId=16208",
             "16x16":"https://jira.example.net/secure/useravatar?size=xsmall&ownerId=joschka.schulz%40example.com&avatarId=16208",
             "32x32":"https://jira.example.net/secure/useravatar?size=medium&ownerId=joschka.schulz%40example.com&avatarId=16208"
          },
          "displayName":"User2",
          "active":true,
          "timeZone":"Europe/Berlin"
       },
       "subtasks":[
          
       ],
       "reporter":{
          "self":"https://jira.example.net/rest/api/2/user?username=user2%40example.com",
          "name":"user2@example.com",
          "key":"user2@example.com",
          "emailAddress":"user2@example.com",
          "avatarUrls":{
             "48x48":"https://jira.example.net/secure/useravatar?ownerId=user2%40example.com&avatarId=2",
             "24x24":"https://jira.example.net/secure/useravatar?size=small&ownerId=joschka.schulz%40example.com&avatarId=2",
             "16x16":"https://jira.example.net/secure/useravatar?size=xsmall&ownerId=joschka.schulz%40example.com&avatarId=2",
             "32x32":"https://jira.example.net/secure/useravatar?size=medium&ownerId=joschka.schulz%40example.com&avatarId=2"
          },
          "displayName":"User2",
          "active":true,
          "timeZone":"Europe/Berlin"
       },
       "aggregateprogress":{
          "progress":0,
          "total":0
       },
       "progress":{
          "progress":0,
          "total":0
       },
       "worklog":{
          "startAt":0,
          "maxResults":20,
          "total":0,
          "worklogs":[
             
          ]
       },
       "issuetype":{
          "self":"https://jira.example.net/rest/api/2/issuetype/10001",
          "id":"10001",
          "description":"Von Jira Software erstellt – nicht bearbeiten oder löschen. Vorgangstyp für eine User Story.",
          "iconUrl":"https://jira.example.net/secure/viewavatar?size=xsmall&avatarId=11415&avatarType=issuetype",
          "name":"Story",
          "subtask":false,
          "avatarId":3
       },
       "timespent":null,
       "project":{
          "self":"https://jira.example.net/rest/api/2/project/1",
          "id":"1",
          "key":"SCIP",
          "name":"SCIP",
          "projectTypeKey":"software",
          "avatarUrls":{
             "48x48":"https://jira.example.net/secure/projectavatar?avatarId=11424",
             "24x24":"https://jira.example.net/secure/projectavatar?size=small&avatarId=11424",
             "16x16":"https://jira.example.net/secure/projectavatar?size=xsmall&avatarId=11424",
             "32x32":"https://jira.example.net/secure/projectavatar?size=medium&avatarId=11424"
          },
          "projectCategory":{
             "self":"https://jira.example.net/rest/api/2/projectCategory/10000",
             "id":"10000",
             "description":"Whatever.",
             "name":"Versicherung (Insurance)"
          }
       },
       "resolutiondate":null,
       "workratio":-1,
       "watches":{
          "self":"https://jira.example.net/rest/api/2/issue/SCIP-1000/watchers",
          "watchCount":1,
          "isWatching":false
       },
       "created":"2022-11-29T17:55:30.000+0100",
       "updated":"2023-08-30T13:25:19.000+0200",
       "timeoriginalestimate":null,
       "description":null,
       "timetracking":{
          
       },
       "attachment":[
          {
             "self":"https://jira.example.net/rest/api/2/attachment/2",
             "id":"104806",
             "filename":"image-2022-11-29-17-31-09-455.png",
             "author":{
                "self":"https://jira.example.net/rest/api/2/user?username=user2%40example.com",
                "name":"user2@example.com",
                "key":"user2@example.com",
                "emailAddress":"user2@example.com",
                "avatarUrls":{
                   "48x48":"https://jira.example.net/secure/useravatar?ownerId=user2%40example.com&avatarId=1",
                   "24x24":"https://jira.example.net/secure/useravatar?size=small&ownerId=user2%40example.com&avatarId=2",
                   "16x16":"https://jira.example.net/secure/useravatar?size=xsmall&ownerId=user2%40example.com&avatarId=2",
                   "32x32":"https://jira.example.net/secure/useravatar?size=medium&ownerId=user2%40example.com&avatarId=16208"
                },
                "displayName":"User2",
                "active":true,
                "timeZone":"Europe/Berlin"
             },
             "created":"2022-11-29T17:31:10.000+0100",
             "size":95917,
             "mimeType":"image/png",
             "content":"https://jira.example.net/secure/attachment/2/image-2022-11-29-17-31-09-455.png",
             "thumbnail":"https://jira.example.net/secure/thumbnail/2/_thumb_104806.png"
          }
       ],
       "summary":"Whatever",
       "comment":{
          "comments":[
             
          ],
          "maxResults":0,
          "total":0,
          "startAt":0
       }
    }
  }`

  httpmock.RegisterResponder(
    "POST",
    "/rest/gadget/1.0/login",
    httpmock.NewStringResponder(200, ""),
  )
  httpmock.RegisterResponder(
    "GET",
    "/rest/api/2/issue/SCIP-1000",
    httpmock.NewStringResponder(200, response),
  )

  jiraClient := GetJiraClient(configJira)
  expectedResult := GetJiraTicketSmmary(configJira, "SCIP-1000", jiraClient)
  assert.Equal(t, "Whatever", expectedResult)
}
