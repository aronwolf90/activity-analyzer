package main

import (
  "strings"
  "os"
  "os/exec"
  "testing"

  "github.com/stretchr/testify/assert"
)

func TestMainWithoutArgs(t *testing.T) {
  if os.Getenv("MAIN") == "1" {
    oldArgs := os.Args
    defer func() { os.Args = oldArgs }()
    os.Args = []string{"main"}
    main()
    return
  }

  var outbuf, errbuf strings.Builder
  cmd := exec.Command(os.Args[0], "-test.run=TestMainWithoutArgs")
  cmd.Env = append(os.Environ(), "MAIN=1")
  cmd.Stdout = &outbuf
  cmd.Stderr = &errbuf
  err := cmd.Run()
  if e, ok := err.(*exec.ExitError); ok && !e.Success() {
      assert.Equal(t, errbuf.String(), "Wrong arguments, use: activity-analyzer YYYY-MM-DD")
      return
  }
  t.Fatalf("process ran with err %v, want exit status 1", err)
}

func TestMainWithWrongArgs(t *testing.T) {
  if os.Getenv("MAIN") == "1" {
    oldArgs := os.Args
    defer func() { os.Args = oldArgs }()
    os.Args = []string{"main", "01.01.2022"}
    main()
    return
  }

  var outbuf, errbuf strings.Builder
  cmd := exec.Command(os.Args[0], "-test.run=TestMainWithWrongArgs")
  cmd.Env = append(os.Environ(), "MAIN=1")
  cmd.Stdout = &outbuf
  cmd.Stderr = &errbuf
  err := cmd.Run()
  if e, ok := err.(*exec.ExitError); ok && !e.Success() {
      assert.Equal(t, errbuf.String(), "Wrong arguments, use: activity-analyzer YYYY-MM-DD")
      return
  }
  t.Fatalf("process ran with err %v, want exit status 1", err)
}
