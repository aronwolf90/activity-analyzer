FROM golang

ENV SHELL=zsh
ENTRYPOINT ["zsh", "-c"]

RUN apt-get update -y; apt-get install tmux fuse silversearcher-ag ripgrep xclip zsh python3-pip universal-ctags -y

RUN curl -LO https://github.com/neovim/neovim/releases/latest/download/nvim.appimage && \
  chmod u+x nvim.appimage && \
  ./nvim.appimage --appimage-extract && \
  mv squashfs-root / && \
  ln -s /squashfs-root/AppRun /usr/local/bin/nvim

RUN sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
RUN pip3 install --break-system-packages neovim-remote

WORKDIR /app
