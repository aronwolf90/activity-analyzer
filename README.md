# activity-analyzer

This is a alpha version!!!!!

This is a personal project that I started to easier track my done work, so that I have it
easer to enter my done work on the time tracking tool of the company where I work.

Contributions are welcome.

## Getting started

* Install go
* create a file named `~/.config/activity-analyzer.yaml`
  ```yaml
  time_zone: Europe/Berlin
  jira:
    url: <url>
    username: <username>
    password: <password> 
  gitlab:
    url: <gitlab>
    access_token: <access_token> 
  outlock:
    client_id: <client_id>
    tenant_id: common
  ```
* Run `go run $(ls -1 src/*.go | grep -v _test.go) YYYY-MM-DD`

## Testing
* Install go
* Execute:
  * `cd src`
  * `go test`

## Install
`curl https://gitlab.com/api/v4/projects/48842627jobs/artifacts/main/download?job=build -o /usr/local/bin/activity-analyzer && chmod a+xrw /usr/local/bin/activity-analyzer`

## Next steps
* Add build (using .gitlab-ci.yml)
* Add Teams
